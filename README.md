## How to build and run
* Clone the repo

* Load db file into mysql database

* `cd mail`

* `./gradlew build`

* `./gradlew run`

* Open [link](http://localhost:9090/registration) in the browser

* Add couple of users

* Go to [login page](http://localhost:9090/login) and login any user

* View/Compose emails

## Database schema

* Refer sql file at location - `mail/src/main/resources/sql/init-db.sql`

## Languages/Frameworks used

* Spring Boot, Thymeleaf, MySql, JAVA, HTML, CSS
