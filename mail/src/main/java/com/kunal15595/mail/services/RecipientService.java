package com.kunal15595.mail.services;

import com.kunal15595.mail.entities.Recipient;
import com.kunal15595.mail.repositories.ThreadRepository;
import com.kunal15595.mail.repositories.RecipientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecipientService {

    @Autowired
    private ThreadRepository threadRepository;

    @Autowired
    private RecipientRepository recipientRepository;

    public Recipient saveTo(Recipient recipient) {
        return recipientRepository.save(recipient);
    }

    public void saveRecipients(List<Recipient> recipientList) {
        recipientRepository.save(recipientList);
    }

}
