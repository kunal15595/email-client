package com.kunal15595.mail.services;

import com.kunal15595.mail.entities.Attachment;
import com.kunal15595.mail.entities.Sender;
import com.kunal15595.mail.repositories.AttachmentRepository;
import com.kunal15595.mail.repositories.SenderRepository;
import com.kunal15595.mail.repositories.ThreadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class AttachmentService {

    @Autowired
    private ThreadRepository threadRepository;

    @Autowired
    private AttachmentRepository attachmentRepository;

    public void saveAttachments(List<Attachment> attachmentList) {
        attachmentRepository.save(attachmentList);
    }

}
