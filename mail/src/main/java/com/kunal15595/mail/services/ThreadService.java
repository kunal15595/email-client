package com.kunal15595.mail.services;

import com.google.common.collect.Lists;
import com.kunal15595.mail.entities.*;
import com.kunal15595.mail.entities.Thread;
import com.kunal15595.mail.repositories.ThreadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ThreadService {

    @Autowired
    private UserService userService;

    @Autowired
    private ThreadRepository threadRepository;

    @Autowired
    private RecipientService recipientService;

    @Autowired
    private SenderService senderService;

    @Autowired
    private AttachmentService attachmentService;

    public Thread findById(long id) {
        return threadRepository.findOne(id);
    }

    public List<Thread> findByIds(List<Long> ids) {
        return Lists.newArrayList(threadRepository.findAll(ids));
    }

    public List<Thread> findByMailId(long id) {
        return threadRepository.findByMailIdOrderByTimestampDesc(id);
    }

    public void saveThread(Thread thread, Long mailId) {
        thread.setMailId(mailId);
        saveThread(thread);
    }

    public void saveThread(Thread thread) {

        Thread insertedThread = threadRepository.save(thread);
        long threadId = insertedThread.getThreadId();

        List<String> recipientEmails = Arrays.stream(thread.getRecipients().split(","))
                .map(String::trim)
                .collect(Collectors.toList());

        List<User> recipientUsers = userService.findUsersByEmail(recipientEmails);

        List<Recipient> recipientList = recipientUsers.stream()
                .map(user -> {
                    Recipient recipient = new Recipient();
                    recipient.setId(new Recipient.Recipient_Pk(threadId, user.getId()));
                    return recipient;
                })
                .collect(Collectors.toList());
        recipientService.saveRecipients(recipientList);

        if (thread.getAttachments() != null) {
            List<String> attachmentUrls = Arrays.asList(thread.getAttachments().split(","));

            List<Attachment> attachmentList = attachmentUrls.stream()
                    .map(s -> {
                        Attachment attachment = new Attachment();
                        attachment.setId(new Attachment.Attachment_Pk(threadId, s.trim()));
                        return attachment;
                    })
                    .collect(Collectors.toList());

            attachmentService.saveAttachments(attachmentList);
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());

        Sender sender = new Sender();
        sender.setId(new Sender.Sender_Pk(threadId, user.getId()));

        senderService.saveSender(sender);
    }
}
