package com.kunal15595.mail.services;

import com.kunal15595.mail.entities.Sender;
import com.kunal15595.mail.repositories.SenderRepository;
import com.kunal15595.mail.repositories.ThreadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SenderService {

    @Autowired
    private ThreadRepository threadRepository;

    @Autowired
    private SenderRepository senderRepository;

    public void saveSenders(List<Sender> senderList) {
        senderRepository.save(senderList);
    }

    public void saveSender(Sender sender) {
        senderRepository.save(sender);
    }

}
