package com.kunal15595.mail.services;

import com.kunal15595.mail.entities.*;
import com.kunal15595.mail.entities.Thread;
import com.kunal15595.mail.repositories.MailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class MailService {

    @Autowired
    private UserService userService;

    @Autowired
    private MailRepository mailRepository;

    @Autowired
    private ThreadService threadService;

    @Autowired
    private SenderService senderService;

    @Autowired
    private RecipientService recipientService;

    public Mail findById(long id) {
        return mailRepository.findByMailId(id);
    }

    public Mail insertMail(Thread thread) {

        Mail mail = new Mail();
        mail.setSubject(thread.getSubject());
        mail.setDeleted(false);
        Mail insertedMail = mailRepository.save(mail);

        thread.setMailId(insertedMail.getMailId());
        threadService.saveThread(thread);

        return insertedMail;
    }

    public List<Mail> getAllMailsByUserId(Long userId) {
        return mailRepository.findAllByUserId(userId);
    }

    public List<Mail> getInboxMailsByUserId(Long userId) {
        return mailRepository.findReceivedByUserId(userId);
    }

    public List<Mail> getSentMailsByUserId(Long userId) {
        return mailRepository.findSentByUserId(userId);
    }

    public List<Mail> getTrashMailsByUserId(Long userId) {
        return mailRepository.findDeletedByUserId(userId);
    }

    public void moveMailToTrash(Long mailId) {
        mailRepository.moveToTrash(mailId);
    }
}
