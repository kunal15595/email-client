package com.kunal15595.mail.services;

import com.google.common.collect.Lists;
import com.kunal15595.mail.entities.Role;
import com.kunal15595.mail.entities.User;
import com.kunal15595.mail.repositories.RoleRepository;
import com.kunal15595.mail.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public List<User> findUsersByEmail(List<String> emails) {
        return Lists.newArrayList(userRepository.findByEmailIn(emails));
    }

    public List<User> findUsersById(List<Long> ids) {
        return Lists.newArrayList(userRepository.findAll(ids));
    }

    public void saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Role userRole = roleRepository.findByRole("USER");
        user.setActive(1);
        user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
        userRepository.save(user);
    }
}
