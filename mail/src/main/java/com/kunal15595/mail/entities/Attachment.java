package com.kunal15595.mail.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "attachment")
public class Attachment {

    @Embeddable
    public static class Attachment_Pk implements Serializable {

        @Column(name="thread_id", nullable=false, updatable=false)
        private Long threadId;

        @Column(name="url", nullable=false, updatable=false)
        private String url;

        public Long getThreadId() {
            return threadId;
        }

        public void setThreadId(Long threadId) {
            this.threadId = threadId;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Attachment_Pk(Long threadId, String url) {
            this.threadId = threadId;
            this.url = url;
        }

        public Attachment_Pk() {
        }
    }

    @EmbeddedId
    private Attachment_Pk id;

    public Attachment_Pk getId() {
        return id;
    }

    public void setId(Attachment_Pk id) {
        this.id = id;
    }

}
