package com.kunal15595.mail.entities;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "sender")
public class Sender {

    @Embeddable
    public static class Sender_Pk implements Serializable {

        @Column(name="thread_id")
        private Long threadId;

        @Column(name="user_id")
        private Long userId;

        public Long getThreadId() {
            return threadId;
        }

        public void setThreadId(Long threadId) {
            this.threadId = threadId;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Sender_Pk(Long threadId, Long userId) {
            this.threadId = threadId;
            this.userId = userId;
        }

        public Sender_Pk() {
        }
    }

    @EmbeddedId
    private Sender_Pk id;

    public Sender_Pk getId() {
        return id;
    }

    public void setId(Sender_Pk id) {
        this.id = id;
    }
}
