package com.kunal15595.mail.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "recipient")
public class Recipient {

    @Embeddable
    public static class Recipient_Pk implements Serializable {

        @Column(name="thread_id")
        private Long threadId;

        @Column(name="user_id")
        private Long userId;

        public Long getThreadId() {
            return threadId;
        }

        public void setThreadId(Long threadId) {
            this.threadId = threadId;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Recipient_Pk(Long threadId, Long userId) {
            this.threadId = threadId;
            this.userId = userId;
        }

        public Recipient_Pk() {
        }
    }

    @EmbeddedId
    private Recipient_Pk id;

    public Recipient_Pk getId() {
        return id;
    }

    public void setId(Recipient_Pk id) {
        this.id = id;
    }
}
