package com.kunal15595.mail.entities;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "thread")
public class Thread implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="thread_id")
    private long threadId;

    @Column(name="mail_id")
    private long mailId;

    @Column(name="timestamp")
    private Timestamp timestamp;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "thread_id")
    private List<Recipient> recipientList;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "thread_id")
    private List<Sender> senderList;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "thread_id")
    private List<Attachment> attachmentList;

    @Length(min = 1, message = "*Empty email body not allowed")
    @Column(name="body")
    private String body;

    @Transient
    @Length(min = 1, message = "*Empty email subject not allowed")
    private String subject;

    @Transient
    private String recipients;

    @Transient
    private String attachments;

    public long getThreadId() {
        return threadId;
    }

    public void setThreadId(long threadId) {
        this.threadId = threadId;
    }

    public long getMailId() {
        return mailId;
    }

    public void setMailId(long mailId) {
        this.mailId = mailId;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public List<Recipient> getRecipientList() {
        return recipientList;
    }

    public void setRecipientList(List<Recipient> recipientList) {
        this.recipientList = recipientList;
    }

    public List<Sender> getSenderList() {
        return senderList;
    }

    public void setSenderList(List<Sender> senderList) {
        this.senderList = senderList;
    }

    public List<Attachment> getAttachmentList() {
        return attachmentList;
    }

    public void setAttachmentList(List<Attachment> attachmentList) {
        this.attachmentList = attachmentList;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }
}
