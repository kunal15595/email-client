package com.kunal15595.mail.controllers;

import com.kunal15595.mail.entities.Recipient;
import com.kunal15595.mail.entities.Sender;
import com.kunal15595.mail.entities.Thread;
import com.kunal15595.mail.services.ThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collections;
import java.util.List;

@Controller
public class ThreadController {

    @Autowired
    private ThreadService threadService;

    @RequestMapping(value="/new/thread", method = RequestMethod.GET)
    @ResponseBody
    public Thread newThread(){
        Thread thread = new Thread();
        thread.setBody("");
        return thread;
    }

    @RequestMapping(value="/api/v1/thread/{threadId}", method = RequestMethod.GET)
    @ResponseBody
    public Thread viewThreadApi(
            @PathVariable("threadId") Long threadId
    ){
        return threadService.findById(threadId);
    }

    @RequestMapping(value="/thread/{threadId}", method = RequestMethod.GET)
    public ModelAndView viewThread(
            @PathVariable("threadId") Long threadId
    ){
        ModelAndView modelAndView = new ModelAndView();
        Thread thread = threadService.findById(threadId);
        modelAndView.addObject("thread", thread);
        modelAndView.setViewName("thread");
        return modelAndView;
    }
}
