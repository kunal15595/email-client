package com.kunal15595.mail.controllers;

import com.kunal15595.mail.entities.Mail;
import com.kunal15595.mail.entities.Thread;
import com.kunal15595.mail.entities.User;
import com.kunal15595.mail.services.MailService;
import com.kunal15595.mail.services.ThreadService;
import com.kunal15595.mail.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@Controller
public class MailController {

    @Autowired
    private MailService mailService;

    @Autowired
    private ThreadService threadService;

    @Autowired
    private UserService userService;

    @RequestMapping(value="/api/v1/mail/{mailId}", method = RequestMethod.GET)
    @ResponseBody
    public Mail viewMailApi(
            @PathVariable("mailId") Long mailId
    ){
        return mailService.findById(mailId);
    }

    @RequestMapping(value="/mail/{mailId}", method = RequestMethod.GET)
    public ModelAndView viewMail(
            @PathVariable("mailId") Long mailId
    ){
        ModelAndView modelAndView = new ModelAndView();
        Mail mail = mailService.findById(mailId);
        modelAndView.addObject("mail", mail);

        Thread thread = new Thread();
        thread.setBody("");
        thread.setRecipients("");
        thread.setAttachments("");
        thread.setMailId(mailId);
        modelAndView.addObject("thread", thread);
        modelAndView.setViewName("mail");
        return modelAndView;
    }

    @RequestMapping(value="/api/v1/mail/user/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public List<Mail> viewAllUserMailsApi(
            @PathVariable("userId") Long userId
    ){
        return mailService.getAllMailsByUserId(userId);
    }

    @RequestMapping(value="/mails/user/{userId}", method = RequestMethod.GET)
    public ModelAndView viewAllUserMails(
            @PathVariable("userId") Long userId
    ){
        ModelAndView modelAndView = new ModelAndView();
        List<Mail> mails = mailService.getAllMailsByUserId(userId);
        modelAndView.addObject("mails", mails);
        modelAndView.addObject("userId", userId);
        modelAndView.setViewName("mails");
        return modelAndView;
    }

    @RequestMapping(value="/inbox/user/{userId}", method = RequestMethod.GET)
    public ModelAndView viewUserInboxMails(
            @PathVariable("userId") Long userId
    ){
        ModelAndView modelAndView = new ModelAndView();
        List<Mail> mails = mailService.getInboxMailsByUserId(userId);
        modelAndView.addObject("mails", mails);
        modelAndView.addObject("userId", userId);
        modelAndView.setViewName("mails");
        return modelAndView;
    }

    @RequestMapping(value="/sent/user/{userId}", method = RequestMethod.GET)
    public ModelAndView viewUserSentMails(
            @PathVariable("userId") Long userId
    ){
        ModelAndView modelAndView = new ModelAndView();
        List<Mail> mails = mailService.getSentMailsByUserId(userId);
        modelAndView.addObject("mails", mails);
        modelAndView.addObject("userId", userId);
        modelAndView.setViewName("mails");
        return modelAndView;
    }

    @RequestMapping(value="/trash/user/{userId}", method = RequestMethod.GET)
    public ModelAndView viewUserTrashMails(
            @PathVariable("userId") Long userId
    ){
        ModelAndView modelAndView = new ModelAndView();
        List<Mail> mails = mailService.getTrashMailsByUserId(userId);
        modelAndView.addObject("mails", mails);
        modelAndView.addObject("userId", userId);
        modelAndView.setViewName("mails");
        return modelAndView;
    }

    @RequestMapping(value="/mails", method = RequestMethod.GET)
    public ModelAndView viewAllPersonalMails() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());

        return viewAllUserMails(user.getId());
    }

    @RequestMapping(value = "/mail/{mailId}", method = RequestMethod.POST)
    public ModelAndView createNewMail(
            @Valid Thread thread,
            BindingResult bindingResult,
            @PathVariable("mailId") Long mailId
    ) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("mail");
        if (!bindingResult.hasErrors()) {
            try {
                threadService.saveThread(thread, mailId);
                Thread newMailThread = new Thread();
                newMailThread.setMailId(mailId);
                modelAndView.addObject("thread", newMailThread);
            } catch (IllegalStateException e) {
                modelAndView.addObject("thread", thread);
            }
            modelAndView.addObject("successMessage", "Replied successfully");
            modelAndView.addObject("mail", mailService.findById(mailId));
        }
        return modelAndView;
    }

    @RequestMapping(value = "/trash/{mailId}", method = RequestMethod.POST)
    public String moveMailToTrash(
            @Valid Mail mail,
            BindingResult bindingResult,
            @PathVariable("mailId") Long mailId
    ) {
        if (!bindingResult.hasErrors()) {
            try {
                mailService.moveMailToTrash(mailId);
            } catch (Exception e) {

            }
        }
        return "redirect:/mails";
    }
}
