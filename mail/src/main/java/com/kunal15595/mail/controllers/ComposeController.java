package com.kunal15595.mail.controllers;

import com.kunal15595.mail.entities.Mail;
import com.kunal15595.mail.entities.Thread;
import com.kunal15595.mail.services.MailService;
import com.kunal15595.mail.services.ThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Collections;

@Controller
public class ComposeController {

    @Autowired
    private MailService mailService;

    @Autowired
    private ThreadService threadService;

    @RequestMapping(value="/compose", method = RequestMethod.GET)
    public ModelAndView registration(){
        ModelAndView modelAndView = new ModelAndView();
        Thread thread = new Thread();
        thread.setBody("");
        thread.setRecipients("");
        thread.setAttachments("");
        modelAndView.addObject("thread", thread);
        modelAndView.setViewName("compose");
        return modelAndView;
    }

    @RequestMapping(value = "/send", method = RequestMethod.POST)
    public ModelAndView createNewMail(@Valid Thread thread, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        if (!bindingResult.hasErrors()) {
            try {
                Mail insertedMail = mailService.insertMail(thread);
                modelAndView.addObject("mail", insertedMail);
                Thread newThread = new Thread();
                newThread.setBody("");
                newThread.setRecipients("");
                newThread.setAttachments("");
                newThread.setMailId(insertedMail.getMailId());
                modelAndView.addObject("thread", newThread);
                modelAndView.setViewName("mail");
                modelAndView.addObject("successMessage", "Mail has been sent successfully");
            } catch (IllegalStateException e) {
                modelAndView.setViewName("compose");
                modelAndView.addObject("thread", thread);
            }
        }
        return modelAndView;
    }
}
