package com.kunal15595.mail.repositories;

import com.kunal15595.mail.entities.Attachment;
import com.kunal15595.mail.entities.Sender;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AttachmentRepository extends CrudRepository<Attachment, Long> {

}