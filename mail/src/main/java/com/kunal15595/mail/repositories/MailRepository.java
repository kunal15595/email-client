package com.kunal15595.mail.repositories;

import com.kunal15595.mail.entities.Mail;
import org.hibernate.annotations.SQLUpdate;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MailRepository extends CrudRepository<Mail, Long> {

    Mail findByMailId(long id);

    @Query(value = "SELECT DISTINCT m.mail_id, m.subject, m.is_deleted, m.timestamp FROM mail m JOIN thread t ON m.mail_id=t.mail_id JOIN recipient r ON t.thread_id=r.thread_id JOIN sender s ON s.thread_id=t.thread_id WHERE r.user_id = ?1 OR s.user_id = ?1 ORDER BY m.timestamp DESC ", nativeQuery = true)
    List<Mail> findAllByUserId(long userId);

    @Query(value = "SELECT DISTINCT m.mail_id, m.subject, m.is_deleted, m.timestamp FROM mail m JOIN thread t ON m.mail_id=t.mail_id JOIN sender s ON s.thread_id=t.thread_id WHERE m.is_deleted=0 AND s.user_id = ?1 ORDER BY m.timestamp DESC ", nativeQuery = true)
    List<Mail> findSentByUserId(long userId);

    @Query(value = "SELECT DISTINCT m.mail_id, m.subject, m.is_deleted, m.timestamp FROM mail m JOIN thread t ON m.mail_id=t.mail_id JOIN recipient r ON t.thread_id=r.thread_id WHERE m.is_deleted=0 AND r.user_id = ?1 ORDER BY m.timestamp DESC ", nativeQuery = true)
    List<Mail> findReceivedByUserId(long userId);

    @Query(value = "SELECT DISTINCT m.mail_id, m.subject, m.is_deleted, m.timestamp FROM mail m JOIN thread t ON m.mail_id=t.mail_id JOIN recipient r ON t.thread_id=r.thread_id JOIN sender s ON s.thread_id=t.thread_id WHERE m.is_deleted=1 AND (r.user_id = ?1 OR s.user_id = ?1) ORDER BY m.timestamp DESC ", nativeQuery = true)
    List<Mail> findDeletedByUserId(long userId);

    @Modifying
    @Query(value = "UPDATE mail SET is_deleted=1 WHERE mail_id=?1", nativeQuery = true)
    void moveToTrash(long mailId);
}