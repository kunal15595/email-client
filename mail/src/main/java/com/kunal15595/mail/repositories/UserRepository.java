package com.kunal15595.mail.repositories;

import com.kunal15595.mail.entities.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {

    User findByEmail(String email);

    List<User> findByEmailIn(List<String> emails);
}