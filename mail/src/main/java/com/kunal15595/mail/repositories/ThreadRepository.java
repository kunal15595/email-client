package com.kunal15595.mail.repositories;

import com.kunal15595.mail.entities.Recipient;
import com.kunal15595.mail.entities.Sender;
import com.kunal15595.mail.entities.Thread;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ThreadRepository extends CrudRepository<Thread, Long> {

    List<Thread> findByMailIdOrderByTimestampDesc(long id);

}