DROP DATABASE mailbox;

-- create database
CREATE DATABASE mailbox;
CREATE USER 'superuser'@'%' IDENTIFIED BY 'superpassword';
GRANT ALL ON mailbox.* to 'superuser'@'%';

USE mailbox;

-- create tables
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `role_id` INT(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `active` BIGINT(20) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `user_id` BIGINT(20) NOT NULL,
  `role_id` INT(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`),
  CONSTRAINT `fk_user_role_2_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `fk_user_role_2_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `mail`;
CREATE TABLE `mail` (
  `mail_id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `subject` TEXT NOT NULL,
  `is_deleted` TINYINT(1) NOT NULL DEFAULT 0,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`mail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `thread`;
CREATE TABLE `thread` (
  `thread_id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `mail_id` BIGINT(20) NOT NULL,
  `body` MEDIUMTEXT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`thread_id`),
  CONSTRAINT `fk_thread_2_mail` FOREIGN KEY (`mail_id`) REFERENCES `mail` (`mail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `recipient`;
CREATE TABLE `recipient` (
  `thread_id` BIGINT(20) NOT NULL,
  `user_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`thread_id`,`user_id`),
  CONSTRAINT `fk_recipient_2_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `fk_recipient_2_thread` FOREIGN KEY (`thread_id`) REFERENCES `thread` (`thread_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `sender`;
CREATE TABLE `sender` (
  `thread_id` BIGINT(20) NOT NULL,
  `user_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`thread_id`,`user_id`),
  CONSTRAINT `fk_sender_2_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `fk_sender_2_thread` FOREIGN KEY (`thread_id`) REFERENCES `thread` (`thread_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `attachment`;
CREATE TABLE `attachment` (
  `thread_id` BIGINT(20) NOT NULL,
  `url` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`thread_id`,`url`),
  CONSTRAINT `fk_attachment_2_thread` FOREIGN KEY (`thread_id`) REFERENCES `thread` (`thread_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- insert roles
INSERT INTO `role`(role) VALUES ('ADMIN');
INSERT INTO `role`(role) VALUES ('USER');
INSERT INTO `role`(role) VALUES ('ACTUATOR');
